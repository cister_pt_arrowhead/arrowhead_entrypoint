package eu.arrowhead.common.exception;

public class ConsumerConfigurationException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6493652897295857002L;

	public ConsumerConfigurationException(String message) {
		super(message);
	}

}
