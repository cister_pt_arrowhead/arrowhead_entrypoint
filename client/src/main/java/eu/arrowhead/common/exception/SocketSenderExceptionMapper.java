/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.arrowhead.common.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author Renato
 */
@Provider
public class SocketSenderExceptionMapper implements ExceptionMapper<SocketSenderException> {

	@Override
	public Response toResponse(SocketSenderException ex) {
		ErrorMessage errorMessage = new ErrorMessage(ex.getMessage(), 401, "No documentation yet");
		return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(errorMessage).build();
	}

}
