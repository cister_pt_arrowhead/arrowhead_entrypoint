package eu.arrowhead.common.exception;

public class SocketSenderException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2840666578718982589L;

	public SocketSenderException(String message) {
		super(message);
	}

}
