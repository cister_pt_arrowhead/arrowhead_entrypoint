package eu.arrowhead.common.exception;

public class ProducerConfigurationException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4391036222044458371L;

	public ProducerConfigurationException(String message) {
		super(message);
	}

}
