package eu.arrowhead.common.exception;

public class InsufficientCommandsException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2061911010224187009L;

	public InsufficientCommandsException(String message) {
		super(message);
	}

}
