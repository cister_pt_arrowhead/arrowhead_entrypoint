package eu.arrowhead.common.exception;

public class InvalidCommandException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3289599118295414703L;

	public InvalidCommandException(String message) {
		super(message);
	}

}
