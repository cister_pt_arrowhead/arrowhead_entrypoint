/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.arrowhead.common.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author Renato
 */
@Provider
public class ConsumerConfigurationExceptionMapper implements ExceptionMapper<ConsumerConfigurationException> {

	@Override
	public Response toResponse(ConsumerConfigurationException ex) {
		ErrorMessage errorMessage = new ErrorMessage(ex.getMessage(), 401, "No documentation yet");
		return Response.status(Status.INTERNAL_SERVER_ERROR).entity(errorMessage).build();
	}

}
