package eu.arrowhead.common.configuration;

public interface Properties {

	final String CONSUMER = "CONSUMER##";
	final String PRODUCER = "PRODUCER##";
	final String STREAM = "STREAM##";

	final String ID = "ID";
	final String SIZE = "SIZE";
	final String PERIOD = "PERIOD";
	final String MTU = "MTU";
	final String SYNCHRONOUS = "SYNCHRONOUS";

	final String SUCCESS_RESPONSE = "SUCCESS";

	final String TIME_RESPONSE = "responseTime";
	final String BANDWIDTH = "bandwidth";
	final String DELAY = "delay";

	final String MONITOR = "MONITOR##";

}
