package eu.arrowhead.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import fttse.entrypoint.ExecuteSocketServer;
import javafx.application.Platform;

public class ServletContextClass implements ServletContextListener {

    @Override
    public void contextDestroyed(ServletContextEvent arg0) {
        // TODO Auto-generated method stub
        Platform.exit();
        System.out.println("[Arrowhead Core] Servlet Destroyed.");

    }

    @Override
    public void contextInitialized(ServletContextEvent arg0) {
        System.out.println("[Arrowhead Core] Servlet deployed.");
        ExecuteSocketServer exe = new ExecuteSocketServer();
        new Thread(exe).start();
        System.out.println("[Arrowhead Core] Servlel Goodbye.");
    }
}
