package eu.arrowhead.socket;

import eu.arrowhead.common.exception.SocketSenderException;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SocketSender {
    // private static Map<String, SocketChannel> connections;

    private static Map<String, ArrayList<Object>> connections;

    public SocketSender() {
        if (connections == null) {
            connections = new HashMap<>();
        }
    }

    public String sendSocket(String ip, String port, String content) throws SocketSenderException {
        Socket socket;

        try {
            ArrayList<Object> list = checkIfConnectionExists(ip, port);
            socket = (Socket) list.get(0);
            PrintWriter out = (PrintWriter) list.get(1);
            DataInputStream in = (DataInputStream) list.get(2);

            // DataOutputStream out = new
            // DataOutputStream(socket.getOutputStream());
            out.println(content);
            in.read();

            socketClose(socket, ip, port);
            // Now lets receive the response

        } catch (Exception e) {
            String a = "adasd";
            return "FAIL" + ip + ":" + port;
        }

        return "SUCCESSFUL";
    }

    private ArrayList<Object> checkIfConnectionExists(String ip, String port) throws UnknownHostException, IOException {
        ArrayList<Object> list = connections.get(ip + port);

        if (list == null) {
            list = new ArrayList<>();
            Socket s = new Socket(ip, Integer.parseInt(port));

            PrintWriter out = new PrintWriter(s.getOutputStream(), true); // printwriter
            // from
            // socket
            DataInputStream in = new DataInputStream(s.getInputStream());

            list.add(0, s);
            list.add(1, out);
            list.add(2, in);

            connections.put(ip + port, list);
        }

        return list;
    }

    private Boolean socketClose(Socket s, String ip, String port) {
        try {
            s.close();
            connections.remove(ip + port);
            return true;
        } catch (IOException e) {
            return false;
        }

    }

}
