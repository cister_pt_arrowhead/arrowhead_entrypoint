package eu.arrowhead.resources;

import eu.arrowhead.common.model.messages.QoSReservationCommand;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

@Path("manager")
@Produces(MediaType.TEXT_PLAIN)
public class Manager {

    private final ManagerService service = new ManagerService();

    @GET
    @Path("home")
    public Response home() {
        return Response.status(Status.ACCEPTED).entity(new String("Online!")).build();
    }

    @POST
    @Path("configure")
    @Produces(MediaType.APPLICATION_JSON)
    public Response configurate(QoSReservationCommand commands) {
        service.configure(commands.getConsumer(), commands.getProducer(), commands.getCommands(), commands.getRequestedQoS());
        return Response.status(Response.Status.ACCEPTED).build();
    }

    @PUT
    @Path("stop")
    @Produces(MediaType.APPLICATION_JSON)
    public Response stop() {
        return null;
    }

}
