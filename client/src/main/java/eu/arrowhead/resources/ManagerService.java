package eu.arrowhead.resources;

import eu.arrowhead.common.configuration.Properties;
import eu.arrowhead.common.exception.ConsumerConfigurationException;
import eu.arrowhead.common.exception.InsufficientCommandsException;
import eu.arrowhead.common.exception.InvalidCommandException;
import eu.arrowhead.common.exception.ProducerConfigurationException;
import eu.arrowhead.common.exception.SocketSenderException;
import eu.arrowhead.common.model.ArrowheadSystem;
import eu.arrowhead.socket.SocketSender;
import java.util.Map;

public class ManagerService {

    public ManagerService() {

    }

    /**
     *
     * @param consumer
     * @param producer
     * @param commands
     * @param requestedQoS
     * @return
     * @throws ProducerConfigurationException
     * @throws ConsumerConfigurationException
     */
    public boolean configure(ArrowheadSystem consumer, ArrowheadSystem producer, Map<String, String> commands,
            Map<String, String> requestedQoS) {

        if (!configureSlave(Properties.PRODUCER, producer, commands, requestedQoS)) {
            throw new ProducerConfigurationException("There was an error during the Producer Stream Configuration");
        }

        if (!configureSlave(Properties.CONSUMER, consumer, commands, requestedQoS)) {
            throw new ConsumerConfigurationException("There was an error during the Consumer Stream Configuration");
        }

        return true;
    }

    private boolean configureSlave(String slaveType, ArrowheadSystem system, Map<String, String> commands,
            Map<String, String> requestedQoS) {
        try {
            String messageContent;

            if (slaveType.equalsIgnoreCase(Properties.PRODUCER)) {
                messageContent = generateMessage(true, commands);
            } else {
                messageContent = generateMessage(false, commands);
            }

            messageContent += generateRequestedQoSForSlave(requestedQoS);

            SocketSender s = new SocketSender();
            String response = s.sendSocket(system.getAddress(), system.getPort(), messageContent);
            System.out.println("Response: " + response);
            return handleResponse(response);
        } catch (SocketSenderException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        } catch (InsufficientCommandsException | InvalidCommandException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }
    }

    private String generateRequestedQoSForSlave(Map<String, String> requestedQoS) {
    	String out = "\n" + Properties.MONITOR ;
        String bandwidth = requestedQoS.get(Properties.BANDWIDTH);
    	if(bandwidth != null){
    		out+= "\n" + Properties.BANDWIDTH + " " + bandwidth ;
    	}
        String delay = requestedQoS.get(Properties.DELAY);
        if(delay != null){
        	out+=  "\n" + Properties.DELAY + " "
                    + delay ;
        }
        String timeresponse = requestedQoS.get(Properties.TIME_RESPONSE);
        if(timeresponse != null){
        	out+= "\n" + Properties.TIME_RESPONSE + " " + timeresponse;
        }

        return out;
    }

    private String generateMessage(Boolean isProducer, Map<String, String> commands)
            throws InsufficientCommandsException, InvalidCommandException {
        String id = commands.get(Properties.ID);
        String mtu = commands.get(Properties.MTU);
        String period = commands.get(Properties.PERIOD);
        String size = commands.get(Properties.SIZE);
        String synchronous = commands.get(Properties.SYNCHRONOUS);

        String out;

        if (id == null || period == null || synchronous == null || size == null) {
            throw new InsufficientCommandsException("MISSING ID, PERIOD, SYNCHRONOUS, SIZE");
        }

        try {
            Integer.parseInt(id);
            Double.parseDouble(period);
            Double.parseDouble(size);
            Boolean.parseBoolean(synchronous);
        } catch (NumberFormatException e) {
            throw new InvalidCommandException("Invalid Command");
        }
        if (isProducer) {
            out = "\n" + Properties.PRODUCER;
        } else {
            out = "\n" + Properties.CONSUMER;
        }

        out += "\n" + Properties.ID + " " + id + "\n" + Properties.PERIOD + " " + period
                + " " + mtu + "\n" + Properties.SIZE + " " + size + "\n"
                + Properties.SYNCHRONOUS + " " + synchronous;

        return out;
    }

    private boolean handleResponse(String response) {
        return !response.equalsIgnoreCase("ERROR");
    }

}
