/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fttse.entrypoint;

import com.google.gson.JsonSyntaxException;
import fttse.entrypoint.usecase.DeleteService;
import fttse.entrypoint.usecase.HandleEvent;
import fttse.entrypoint.usecase.OrchestrateService;
import fttse.entrypoint.usecase.QoSMonitorLogService;
import fttse.entrypoint.usecase.RegistrateService;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author Barbosa
 */
public class MessageProcessing {

	public MessageProcessing() {

	}

	/**
	 * .
	 *
     * @param out
	 * @param message
	 * @throws IOException
	 */
	public void messageProcessing(PrintWriter out, String message) throws IOException, JsonSyntaxException {
		String identi = "##";
		int index = message.indexOf(identi);
		if (index == -1) {
			return;
		}
		String id = message.substring(0, index);

		message = message.substring(index + identi.length()).trim();
		switch (id.trim()) {
			case "SERVICE_ERROR":
				// System.out.println("error");
				new HandleEvent(message).run();
				break;
			case "SERVICE_REGISTERING":
				//if (new RegistrateService(message).run()) {
					out.println("200");
				/*} else {
					out.println("400");
				}*/
				break;
			case "SERVICE_DELETING":
				new DeleteService(message).run();
				break;
			case "SERVICE_ORCHESTRATION":
				//if (new OrchestrateService(out, message).run()) {
					out.println("200");
				//} else {
				//	out.println("400");
				break;
			case "SERVICE_MONITORING":
				new QoSMonitorLogService(message).run();
				break;
			default:
				System.out.println("Argument Error!");
				break;
		}
	}

}
