/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fttse.entrypoint.common.messages;

/**
 *
 * @author Paulo
 */
import java.util.Map;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class LogParameters {

	private Map<String, String> parameters;

	public LogParameters(Map<String, String> parameters) {
		this.parameters = parameters;
	}

	public Map<String, String> getParameters() {
		return parameters;
	}

	public void setParameters(Map<String, String> parameters) {
		this.parameters = parameters;
	}

}
