/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fttse.entrypoint.common.messages;

import eu.arrowhead.common.model.ArrowheadSystem;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Paulo
 */
public class ProtocolMonitorLogList {

    private List<ProtocolMonitorLog> logs = new ArrayList<>();

    public ProtocolMonitorLogList() {
    }

    public ProtocolMonitorLogList(ArrowheadSystem system, String source,
            List<ProtocolMonitorLog> logs) {
        this.logs = logs;
    }

    public List<ProtocolMonitorLog> getLogs() {
        return logs;
    }

    public void setLogs(List<ProtocolMonitorLog> logs) {
        this.logs = logs;
    }
    
    public void add_log(ProtocolMonitorLog log){
    	this.logs.add(log);
    }

}
