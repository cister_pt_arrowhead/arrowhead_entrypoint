/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fttse.entrypoint.common.messages;

import eu.arrowhead.common.model.messages.ServiceRegistryEntry;
import java.util.List;

/**
 *
 * @author ID0084D
 */
public class FTTSERegistryEntry {

    private String serviceGroup;
    private String serviceDefinition;
    private List<String> interfaces;

    private ServiceRegistryEntry serviceRegistryEntry;

    public FTTSERegistryEntry() {
    }

    public FTTSERegistryEntry(String serviceGroup, String serviceDefinition, List<String> interfaces, ServiceRegistryEntry serviceRegistryEntry) {
        this.serviceGroup = serviceGroup;
        this.serviceDefinition = serviceDefinition;
        this.interfaces = interfaces;
        this.serviceRegistryEntry = serviceRegistryEntry;
    }

    public String getServiceGroup() {
        return serviceGroup;
    }

    public void setServiceGroup(String serviceGroup) {
        this.serviceGroup = serviceGroup;
    }

    public String getServiceDefinition() {
        return serviceDefinition;
    }

    public void setServiceDefinition(String serviceDefinition) {
        this.serviceDefinition = serviceDefinition;
    }

    public List<String> getInterfaces() {
        return interfaces;
    }

    public void setInterfaces(List<String> interfaces) {
        this.interfaces = interfaces;
    }

    public ServiceRegistryEntry getServiceRegistryEntry() {
        return serviceRegistryEntry;
    }

    public void setServiceRegistryEntry(ServiceRegistryEntry serviceRegistryEntry) {
        this.serviceRegistryEntry = serviceRegistryEntry;
    }

}
