/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fttse.entrypoint.common.messages;

import eu.arrowhead.common.model.ArrowheadSystem;

import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ID0084D
 */
@XmlRootElement
public class ProtocolMonitorLog {

    private ArrowheadSystem system;
    private String source;

    private Map<String, String> map;

    /**
     * Creates a new instance with no parameters initialized.
     */
    public ProtocolMonitorLog() {
    	 this.map = new HashMap<>();
    }

    /**
     * Creates a new instance with the given monitor parameters.
     *
     * @param parameters the monitor parameters. It works by getting the value
     * of the parameter (key) e.g. key=bandwidth, value=100
     */
    public ProtocolMonitorLog(Map<String, String> parameters) {
        this.map = parameters;
    }
    

	public ProtocolMonitorLog(ArrowheadSystem arwd_sys, String source) {
		this.system = arwd_sys;
		this.source = source;
   	 this.map = new HashMap<>();

	}

    /**
     * Gets the monitor system
     *
     * @return the monitor system
     */
    public ArrowheadSystem getSystem() {
        return system;
    }

    /**
     * Sets the monitor system
     *
     * @param system the monitor system
     */
    public void setSystem(ArrowheadSystem system) {
        this.system = system;
    }

    /**
     * Gets the monitor source
     *
     * @return the monitor source
     */
    public String getSource() {
        return source;
    }

    /**
     * Sets the monitor source
     *
     * @param source the monitor source
     */
    public void setSource(String source) {
        this.source = source;
    }

    /**
     * Gets the monitor parameters
     *
     * @return the monitor parameters
     */
    public Map<String, String> getParameters() {
        return map;
    }

    /**
     * Sets the monitor parameters.
     *
     * @param parameters the monitor parameters
     */
    public void setParameters(Map<String, String> parameters) {
        this.map = parameters;
    }

}
