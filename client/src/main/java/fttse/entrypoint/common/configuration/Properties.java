/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fttse.entrypoint.common.configuration;

/**
 *
 * @author Paulo
 */
public interface Properties {

    static final int MAX_BUFFER_SIZE = 2024;

    static final String CORE_SERVICE_ORCHESTRATOR_URL = "http://localhost:8444/orchestrator";
    static final String CORE_SERVICE_REGISTRY_URL = "http://arrowhead2.tmit.bme.hu:8444/serviceregistry";
    static final String CORE_SERVICE_QOS_MONITOR_URL = "http://localhost:8144/qosmonitor/Monitor";

    static final String ROUTE_ORCHESTRATION = "/orchestration";
    static final String ROUTE_MONITOR_LOG = "/QoSLog";
    static final String ROUTE_MONITOR_SERVICE_ERROR = "/Event";


    static final int DEFAULT_PORT = 9999;

}
