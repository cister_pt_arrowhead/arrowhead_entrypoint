/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fttse.entrypoint.usecase;

import com.google.gson.Gson;
import eu.arrowhead.common.model.ArrowheadSystem;
import eu.arrowhead.common.model.messages.OrchestrationResponse;
import eu.arrowhead.common.model.messages.ServiceRequestForm;
import fttse.entrypoint.common.configuration.Properties;
import java.io.PrintWriter;
import java.net.URI;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

/**
 *
 * @author Paulo
 */
public class OrchestrateService implements IUseCase {

	private final String content;
	private PrintWriter out;

	/**
	 *
	 * @param content JSON String
	 */
	public OrchestrateService(PrintWriter out, String content) {
		this.content = content;
		this.out = out;
	}

	@Override
	public Boolean run() {
		System.out.println(content);

		ServiceRequestForm obj;

		obj = new Gson().fromJson(content, ServiceRequestForm.class);

		if (obj != null && obj.getOrchestrationFlags() != null
			&& obj.getRequestedService() != null
			&& obj.getRequesterSystem() != null) {

			if (orchestrateService(obj)) {
				System.out.println("Orchestration Success");
				return true;
			} else {
				System.out.println("Orchestration failure");
				return false;
			}
		} else {
			System.out.println("Invalid Arguments");
		}
		return false;
	}

	/**
	 *
	 * @param serviceRegistryEntry
	 * @return
	 */
	private static boolean orchestrateService(
		ServiceRequestForm serviceRequestForm) {
		System.out.println("Orchestrating...");

		Client client = ClientBuilder.newClient();

		URI uri = UriBuilder.
			fromPath(Properties.CORE_SERVICE_ORCHESTRATOR_URL + Properties.ROUTE_ORCHESTRATION).
			build();

		WebTarget target = client.target(uri);

		Response response = target.request()
			.header("Content-type", "application/json")
			.post(Entity.json(serviceRequestForm));

		int responseStatus = response.getStatus();

		if (199 < responseStatus && responseStatus < 300) {
			OrchestrationResponse orchResponse = response.
				readEntity(OrchestrationResponse.class);

			client.close();

			if (orchResponse.getResponse() != null && orchResponse.getResponse().
				size() > 0) {
				ArrowheadSystem system = orchResponse.getResponse().get(0).
					getProvider();

				System.out.
					println("Success:\n " + system.getAddress() + ":" + system.
						getPort() + "/" + orchResponse.getResponse().get(0).
						getServiceURI());
				return true;
			}
		}

		client.close();

		return false;
	}
}
