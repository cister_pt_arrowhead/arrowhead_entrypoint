/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fttse.entrypoint.usecase;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import eu.arrowhead.common.model.ArrowheadSystem;
import eu.arrowhead.common.model.messages.AddMonitorLog;
import fttse.entrypoint.common.configuration.Properties;
import fttse.entrypoint.common.messages.ProtocolMonitorLog;
import fttse.entrypoint.common.messages.ProtocolMonitorLogList;
import fttse.entrypoint.usecase.monitor.type.FTTSE;
import java.net.URI;
import java.util.AbstractQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

/**
 *
 * @author ID0084D
 */
public class QoSMonitorLogService implements IUseCase {

    private final String content;
    private final FTTSE monitor;
    private static final ConcurrentMap<Integer, AbstractQueue<Number>> CHART_DATA = new ConcurrentHashMap();

    /**
     *
     * @param content JSON String
     */
    public QoSMonitorLogService(String content) {
        this.content = content;
        this.monitor = new FTTSE();
    }

    /**
     *
     * @return
     */
    @Override
    public Boolean run() {
    	//
        ProtocolMonitorLogList logs;
        try {
        	logs = deserialize(this.content);  
        	
            for (ProtocolMonitorLog log : logs.getLogs()) {
            	AddMonitorLog message = monitor.filterLogMessage(log);
                if (message != null) {
                    /*if (sendQoSMonitorLog(message)) {
                        System.out.println("Successful in sending ProtocolMonitorLogList");
                    } else {
                        System.out.println("Unsuccessful in sending QoSMonitorLog");
                    }*/
                	System.out.println(">> Sending Log To QoSMonitor:");
                }
            }
            
        } catch (JsonSyntaxException ex) {
            System.out.println(ex.toString() + " Error mapping content into QoSMonitorLog");
        } catch (InterruptedException ex) {
            System.out.println("Error: " + ex.toString());
        }
        return true;
    }

    public static ConcurrentMap<Integer, AbstractQueue<Number>> getChartData() {
        return CHART_DATA;
    }

    public static boolean sendQoSMonitorLog(AddMonitorLog log) {
        Client client = ClientBuilder.newClient();

        URI uri = UriBuilder.
                fromPath(Properties.CORE_SERVICE_QOS_MONITOR_URL
                        + Properties.ROUTE_MONITOR_LOG).
                build();

        WebTarget target = client.target(uri);

        Response response = target.request()
                .header("Content-Type", "application/json")
                .post(Entity.json(log));

        int responseStatus = response.getStatus();

        client.close();

        return (199 < responseStatus && responseStatus < 300);
    }
    
    
	public static ProtocolMonitorLogList deserialize(String message) {
		/*
		 * "system":{"systemGroup":"Ps","systemName":"P1","address":"192.168.60.93","port":"9996","authenticationInfo":"noAuth"};0,1,58,4,1488385312,665692329;0,1,59,4,1488385312,705765311;0,1,60,4,1488385312,745702578;0,1,61,4,1488385312,785760859;0,1,62,4,1488385312,825630952;
		 */
		message = message.substring(1);
		
		System.out.println(">> deserialize: " + message.substring(1));
		String[] logs = message.split(";");
		ArrowheadSystem arwd_sys = new Gson().fromJson(logs[0], ArrowheadSystem.class);
		
		ProtocolMonitorLogList temp = new ProtocolMonitorLogList();

		for (int i = 1; i < logs.length; i++) {

			String[] log_elements = logs[i].split(",");
			//System.out.println(log_elements[0]);

			boolean is_producer = true;
			String source;
			if (log_elements[0].equalsIgnoreCase("1")) {
				source = "p";
			} else {
				source = "c";
				is_producer = false;
			}
			//System.out.println(source);

			ProtocolMonitorLog temp_log = new ProtocolMonitorLog(arwd_sys, source);

			double epoch_time = 1000 * (Double.parseDouble(log_elements[4]));
			epoch_time += (Double.parseDouble(log_elements[5])) / 1000000;

			if (is_producer == true) {
				temp_log.getParameters().put("si", log_elements[1]);
				temp_log.getParameters().put("sn", log_elements[2]);
				temp_log.getParameters().put("stc", "true");
				temp_log.getParameters().put("st", "" + epoch_time);

			} else {

				temp_log.getParameters().put("si", log_elements[1]);
				temp_log.getParameters().put("sn", log_elements[2]);
				temp_log.getParameters().put("rt", "" + epoch_time);
				temp_log.getParameters().put("cs", log_elements[5]);
			}
			temp.add_log(temp_log);
		}
		System.out.println(temp.getLogs().size());

		return temp;
	}
}
