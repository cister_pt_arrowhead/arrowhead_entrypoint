package fttse.entrypoint.usecase;

import java.net.URI;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import com.google.gson.Gson;

import eu.arrowhead.common.model.ArrowheadSystem;
import eu.arrowhead.common.model.messages.OrchestrationResponse;
import eu.arrowhead.common.model.messages.ServiceRequestForm;
import fttse.entrypoint.common.configuration.Properties;
import fttse.entrypoint.common.messages.ServiceError;

public class HandleEvent implements IUseCase {

	private final String content;

	public HandleEvent(String content) {
		super();
		this.content = content;
	}
	
	public String getContent() {
		return content;
	}

	@Override
	public Boolean run() {
		// send to qosmonitor this message
		System.out.println(content);
		ServiceError obj;
		obj = new Gson().fromJson(content, ServiceError.class);
		obj.setType("FTTSE");
		return comunicateError(obj);
	}
	
	/**
	 *
	 * @param serviceRegistryEntry
	 * @return
	 */
	private static boolean comunicateError(
			ServiceError serviceError) {
		System.out.println("Comunicating Error...");

		Client client = ClientBuilder.newClient();

		URI uri = UriBuilder.
			fromPath(Properties.CORE_SERVICE_QOS_MONITOR_URL + Properties.ROUTE_MONITOR_SERVICE_ERROR).
			build();

		WebTarget target = client.target(uri);

		Response response = target.request()
			.header("Content-type", "application/json")
			.post(Entity.json(serviceError));

		int responseStatus = response.getStatus();

		if (199 < responseStatus && responseStatus < 300) {
				client.close();
				return true;
		}

		client.close();

		return false;
	}
}
