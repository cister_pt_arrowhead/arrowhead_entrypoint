/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fttse.entrypoint.usecase.monitor.type.model;

import eu.arrowhead.common.model.messages.AddMonitorLog;
import fttse.entrypoint.usecase.QoSMonitorLogService;
import java.util.HashMap;
import java.util.concurrent.Semaphore;

/**
 *
 * @author ID0084D
 */
public class BandwidthCalculationHelper {

    private final Semaphore semaphore;
    private Double value;
    private final AddMonitorLog defaultLog;

    public BandwidthCalculationHelper(AddMonitorLog defaultLog, Double value) {
        semaphore = new Semaphore(1);
        this.value = value;
        this.defaultLog = defaultLog;
    }

    public Semaphore getSemaphore() {
        return semaphore;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public void addToValue(Double value) {
        this.value += value;
    }

    public void sendBandwidth() {
        try {
            semaphore.acquire();
        } catch (InterruptedException ex) {
            System.out.println("Unable to acquire semaphore! \n\tProvider: "
                    + defaultLog.getProvider() + "\n\tConsumer: " + defaultLog.getConsumer());
        }
        double tempValue = value;
        value = 0.0;
        semaphore.release();
        tempValue /= 1000;
        defaultLog.setParameters(new HashMap<>());
        defaultLog.getParameters().put("bandwidth", Double.toString(tempValue));
        defaultLog.setTimestamp(System.currentTimeMillis());
        Runnable r = () -> {
            QoSMonitorLogService.sendQoSMonitorLog(defaultLog);
        };
        r.run();
    }

}
