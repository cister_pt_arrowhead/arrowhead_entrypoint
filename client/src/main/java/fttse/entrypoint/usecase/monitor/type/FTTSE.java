package fttse.entrypoint.usecase.monitor.type;

import eu.arrowhead.common.model.messages.AddMonitorLog;
import fttse.entrypoint.common.messages.ProtocolMonitorLog;
import fttse.entrypoint.usecase.monitor.type.model.BandwidthCalculationHelper;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class FTTSE {

//    public static final Logger LOG = Logger.getLogger(FTTSE.class.getName());
//    private static final ConcurrentMap<Integer, Map<Integer, ProtocolMonitorLog>> PRODUCER_LOGS = new ConcurrentHashMap<>();
//    private static final ConcurrentMap<Integer, Map<Integer, ProtocolMonitorLog>> CONSUMER_LOGS = new ConcurrentHashMap<>();
    private static final Map<Integer, Map<Integer, ProtocolMonitorLog>> PRODUCER_LOGS = new HashMap();
    private static final Map<Integer, Map<Integer, ProtocolMonitorLog>> CONSUMER_LOGS = new HashMap();
    private static final Map<Integer, BandwidthCalculationHelper> TEMP_CONTENTSIZE = new HashMap<>();
//    private static final Map<Integer, BandwidthCalculationHelper> TEMP_CONTENTSIZE = new ConcurrentHashMap<>();

    private enum Key {

        CONSUMER("c"), PRODUCER("p"),
        SEQUENCENUMBER("sn"), STREAMID("si"),
        SENDTIMECHECK("stc"), SENDTIME("st"), RECEIVETIME("rt"), CONTENTSIZE("cs"),
        BANDWIDTH("bandwidth"), DELAY("delay"), RESPONSETIME("responsetime"),
        PROTOCOL("FTTSE");

        private final String name;

        private Key(String name) {
            this.name = name;
        }
    }

    public FTTSE() {
    }

    public AddMonitorLog filterLogMessage(ProtocolMonitorLog message) throws InterruptedException {

        Map<String, String> params = message.getParameters();
        int streamNumber;
        int sequenceNumber;
        try {
            streamNumber = Integer.valueOf(params.get(Key.STREAMID.name));
        } catch (NumberFormatException ex) {
            System.out.println("Value of parameter "
                    + Key.STREAMID.name + " is not parsable to integer. Please make sure "
                    + "that no invalid characters are present");
//            throw new NumberFormatException("Value of parameter "
//                    + Key.STREAMID.name + " is not parsable to integer. Please make sure "
//                    + "that no invalid characters are present");
            return null;
        }

        try {
            sequenceNumber = Integer.valueOf(params.get(Key.SEQUENCENUMBER.name));
        } catch (NumberFormatException ex) {
            System.out.println("Value of parameter "
                    + Key.SEQUENCENUMBER.name + " is not parsable to integer. Please make sure "
                    + "that no invalid characters are present");
//            throw new NumberFormatException("Value of parameter "
//                    + Key.SEQUENCENUMBER.name + " is not parsable to integer. Please make sure "
//                    + "that no invalid characters are present");
            return null;
        }

        if (sequenceNumber == 0) {
            return null;
        }

        String source = message.getSource();

        if (source.equalsIgnoreCase(Key.CONSUMER.name)) {
            Map<Integer, ProtocolMonitorLog> logs = CONSUMER_LOGS.get(streamNumber);
            if (logs == null) {
                logs = new HashMap();
                CONSUMER_LOGS.put(streamNumber, logs);
            }
            if (logs.containsKey(sequenceNumber)) {
                return null;
            }
            logs.put(sequenceNumber, message);
            if (!(PRODUCER_LOGS.containsKey(streamNumber) && PRODUCER_LOGS.get(streamNumber).containsKey(sequenceNumber))) {
                return null;
            }

//            if (!(QoSMonitorLogService.getChartData().containsKey(streamNumber))) {
//                AbstractQueue<Number> queue = new ConcurrentLinkedQueue();
//                QoSMonitorLogService.getChartData().put(streamNumber, queue);
////                FTTSE_AreaChart.setStream_To_Use(streamNumber);
//                Runnable r;
//                r = () -> {
//                    new FTTSE_AreaChart(streamNumber, queue);
//                };
//                new Thread(r).start();
//            }
            return createQoSMonitorLog(streamNumber, sequenceNumber);

        } else if (source.equalsIgnoreCase(Key.PRODUCER.name)) {
            Map<Integer, ProtocolMonitorLog> logs = PRODUCER_LOGS.get(streamNumber);
            if (logs == null) {
                logs = new HashMap();
                PRODUCER_LOGS.put(streamNumber, logs);
            }
            if (logs.containsKey(sequenceNumber)) {
                return null;
            }
            logs.put(sequenceNumber, message);
            if (!(CONSUMER_LOGS.containsKey(streamNumber) && CONSUMER_LOGS.get(streamNumber).containsKey(sequenceNumber))) {
                return null;
            }
            return createQoSMonitorLog(streamNumber, sequenceNumber);

        } else {
            String exMsg = "Invalid source name: " + source + "\nmake sure to use 'p' for producer or 'c' for consumer";
            System.out.println(exMsg);
//            throw new IllegalArgumentException(exMsg);
            return null;
        }
    }

    private AddMonitorLog createQoSMonitorLog(int stream, int sequence) throws InterruptedException {

        AddMonitorLog log = new AddMonitorLog();
        log.setTimestamp(System.currentTimeMillis());

        ProtocolMonitorLog provider = PRODUCER_LOGS.get(stream).remove(sequence);
        ProtocolMonitorLog consumer = CONSUMER_LOGS.get(stream).remove(sequence);

        Map<String, String> producerParams = provider.getParameters();
        Map<String, String> consumerParams = consumer.getParameters();

        Map<String, String> qosParams = new HashMap<>();

        double delay, sendTime, receiveTime;
        try {
            sendTime = Double.valueOf(producerParams.get(Key.SENDTIME.name));
        } catch (NumberFormatException ex) {
            System.out.println("Value of parameter "
                    + Key.SENDTIME.name + " is not parsable to integer. Please make sure "
                    + "that no invalid characters are present");
            return null;
        }
        try {
            receiveTime = Double.valueOf(consumerParams.get(Key.RECEIVETIME.name));
        } catch (NumberFormatException ex) {
            System.out.println("Value of parameter "
                    + Key.RECEIVETIME.name + " is not parsable to integer. Please make sure "
                    + "that no invalid characters are present");
            return null;
        }
        try {
            delay = Math.abs(sendTime - receiveTime);
        } catch (NumberFormatException ex) {
            System.out.println("Value of parameter "
                    + Key.DELAY.name + " is not parsable to integer. Please make sure "
                    + "that no invalid characters are present");
            return null;
        }

        if (producerParams.containsKey(Key.SENDTIMECHECK.name)) {

            qosParams.put(Key.DELAY.name, Double.toString(sendTime));
        }

        if (consumerParams.containsKey(Key.CONTENTSIZE.name)) {
            Double size = Double.valueOf(consumerParams.get(Key.CONTENTSIZE.name));
            if (!TEMP_CONTENTSIZE.containsKey(stream)) {

                AddMonitorLog defaultLog = new AddMonitorLog();
                defaultLog.setProvider(provider.getSystem());
                defaultLog.setConsumer(consumer.getSystem());
                defaultLog.setProtocol(Key.PROTOCOL.name);

                BandwidthCalculationHelper r = new BandwidthCalculationHelper(defaultLog, size);
                TEMP_CONTENTSIZE.put(stream, r);

                ScheduledExecutorService ses = Executors.newSingleThreadScheduledExecutor();
                ses.scheduleAtFixedRate(() -> {
                    r.sendBandwidth();
                }, 1, 1, TimeUnit.SECONDS);
            } else {
                BandwidthCalculationHelper tempSize = TEMP_CONTENTSIZE.get(stream);

                tempSize.getSemaphore().acquire();

                tempSize.addToValue(size);

                tempSize.getSemaphore().release();
            }
        }

        log.setProtocol(Key.PROTOCOL.name);
        log.setProvider(provider.getSystem());
        log.setConsumer(consumer.getSystem());
        log.setParameters(qosParams);

        return log;
    }

}
