/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fttse.entrypoint.usecase;

import com.google.gson.Gson;
import fttse.entrypoint.common.configuration.Properties;
import fttse.entrypoint.common.messages.FTTSERegistryEntry;
import java.net.URI;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

/**
 *
 * @author Paulo
 */
public class RegistrateService implements IUseCase {

    private final String content;

    /**
     *
     * @param content JSON String
     */
    public RegistrateService(String content) {
        this.content = content;
    }

    /**
     *
     */
    @Override
    public Boolean run() {

        FTTSERegistryEntry fttseEntry;
        fttseEntry = new Gson().fromJson(content, FTTSERegistryEntry.class);

        if (registerProvider(fttseEntry)) {
            System.out.println("Registered");
        } else {
            System.out.println("Error in registry");
        }
        return true;
    }

    /**
     *
     * @param fttseEntry
     * @return
     */
    private static boolean registerProvider(FTTSERegistryEntry fttseEntry) {
        System.out.println("Registering...");
        Client client = ClientBuilder.newClient();

        URI uri = UriBuilder.
                fromPath(Properties.CORE_SERVICE_REGISTRY_URL)
                .path(fttseEntry.getServiceGroup())
                .path(fttseEntry.getServiceDefinition())
                .path(fttseEntry.getInterfaces().get(0)).build();

        WebTarget target = client.target(uri);
        System.out.println(target.getUri().toString());

        Response response = target.request()
                .header("Content-type", "application/json")
                .post(Entity.json(fttseEntry.getServiceRegistryEntry()));

        int responseStatus = response.getStatus();

        client.close();

        return 199 < responseStatus && responseStatus < 300;
    }
}
