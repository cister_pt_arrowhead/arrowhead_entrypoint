package fttse.entrypoint;

import fttse.entrypoint.common.configuration.Properties;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ExecuteSocketServer extends Thread {

	public void run() {
		ServerSocket echoServer = null;
		Socket clientSocket = null;

		try {
			echoServer = new ServerSocket(Properties.DEFAULT_PORT);
		} catch (IOException e) {
			System.out.println(e);
		}

		try {
			while (true) {
				System.out.println("1");
				clientSocket = echoServer.accept();
				new SocketConnection(clientSocket).start();
			}

		} catch (Exception e) {
			System.out.println(e);
		}
	}

}
