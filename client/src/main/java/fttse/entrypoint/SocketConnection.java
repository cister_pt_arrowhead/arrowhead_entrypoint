/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fttse.entrypoint;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 *
 * @author Paulo
 */
public class SocketConnection extends Thread {

	protected Socket socket;

	public SocketConnection(Socket clientSocket) {
		this.socket = clientSocket;
	}

	@Override
	public void run() {
		try {
			BufferedReader input = new BufferedReader(new InputStreamReader(socket.
				getInputStream()));
			PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
			// As long as we receive data, echo that data back to the client.
			System.out.println("Listenning...");

			char[] buffer = new char[1024];

			MessageProcessing msgProc = new MessageProcessing();

			int n = 0;
			while (true) {
				buffer = new char[8192];	
				n = input.read(buffer);
				System.out.println("SocketConnection > 44" + new String(buffer));
				if (n == -1) {
					break;
				}
				msgProc.messageProcessing(out, new String(buffer));
			}
//            while (input.read(buffer) != -1) {
//                msgProc.messageProcessing(new String(buffer));
//                buffer = new char[1024];
//            }

			System.out.println("Closed connection");

			input.close();
			socket.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
